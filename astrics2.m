function [AS, IDX, data_local] = astrics2(data,seed_labels,DR_method,normalizeAS,plot_shape,seeds)
%ASTRICS Generate a graph adjacency matrix AS for data that has
%been grouped into seed clusters specified by seed_labels, with each seed
%cluster represented by a single node (given by seeds) in the graph.
%   
% INPUT:
%   data =  N x M data matrix with rows for N observations and columns for 
%           M variables (dimensions).
%   seed_labels =   N-vector of labels specifying groups of observations
%                   (rows) in data.
%   DR_method = 'PCA', 'centroids+PCA' or 'LDA+PCA'. A string specifying
%               the dimensionality reduction method to use. 'PCA' simply
%               uses the principle components of the data points in the
%               union of each pair of clusters; 'centroids+PCA' maximally
%               separates the two cluster centroids in the first dimension
%               and then uses PCA to find second and third dimensions;
%               'LDA+PCA' uses Fisher's linear discriminant for the first
%               dimension and then uses PCA to find subsequent dimensions.
%               Default is 'LDA+PCA'. If data only has 2 or 3 dimensions,
%               the local dimensionality reduction step of ASTRICS will be
%               completely skipped and the original data in the full number
%               of dimensions (2 or 3) will be used for alpha-shape
%               triangulation.
%   normalizeAS =   true (1) or false (0). Specify whether to normalize all
%                   values in AS to the maximum value, so that the maximum
%                   value in AS is 1. Default is false.
%   plot_shape = true or false (or 1 or 0). Specifies whether or not to
%           plot alpha shapes. Default = false.
%   seeds = K x M matrix, where K is the number of groups of observations,
%           specifying the M-dimensional coordinates of the nodes that
%           represent each observation group (i.e. the centroid of each 
%           group).
%
% OUTPUT:
%   AS =    K x K adjacency matrix defining a weighted graph. The weight of
%           each edge in the graph is determined by calculating a
%           similarity (overlap) score for the corresponding pair of seed 
%           groups in a low dimensional subspace defined by the direction 
%           separating the cluster centroids and principle components for
%           the original data points in the two groups of observations
%           corresponding to the edge.
%   IDX =   The edges of the graph. Each row of IDX contains the indices of
%           a pair of nodes.
%   data_local =    A cell array that stores the local dimension-reduced
%                   representations of each pair of seed clusters (nodes)
%                   corresponding to the edges in IDX.
%
%
% Function written by Joshua Scurll (jscurll.ubc+astrics@gmail.com).
%

%%

nout = nargout;

ndim = size(data,2);

% Exit with an error if data has < 2 dimensions
if ndim < 2
    error("Data must have at least 2 dimensions!")
end

% Make sure that seed_labels is a column vector
if size(seed_labels,2) > 1, seed_labels = seed_labels.'; end

% Make sure that seed_labels starts at 1 and has no gaps in numbering
[~,~,seed_labels] = unique(seed_labels);


%% Identify and remove outliers from each seed cluster
for i = 1:max(seed_labels)
    % Project the data points in seed i onto their principle 3D subspace.
    pts = seed_labels==i;
    % Skip checking for outliers if the seed cluster contains fewer than 8 
    % points. By ensuring that the interquartile range contains at least 4
    % points, this almost guarantees that the seed cluster still inherently
    % occupies at least a 3D volume in the high dimensional space after
    % outliers have been removed.
    if nnz(pts) < 8
        continue;
    end
    X = data(pts,:);
    % Remove any columns with all zero elements
    X = X(:,any(logical(X)));
    if issparse(X)
        % Reduce to 3 dimensions (or ndim if ndim < 3) using SVD
        [~,~,V] = svds(X, min(3,ndim));
        Y = X*V;
    else
        % Reduce to 3 dimensions (or ndim if ndim < 3) using PCA
        [~,Y] = pca(X, 'NumComponents', min(3,ndim));
    end
    clear X;
    % Center and normalize data points from seed i
    YY = (Y - repmat(trimmean(Y,50),size(Y,1),1)) ./ repmat(mad(Y,1),size(Y,1),1);
    % Find the distance of each point from the origin in the centred and 
    % normalized space.
    D = sqrt(sum(YY.^2,2));
    % Identify a set of possible outliers.
    f = sqrt(D);
    Q = quantile(f,[0.25,0.75]);
    out1 = f > Q(2) + 1.5*(Q(2)-Q(1));
    if any(out1)
        % Find the distance of every point from its nearest neighbour within
        % the same seed cluster in the normalized 3D subspace.
        [~,D] = knnsearch(YY,YY,'k',2);
        D = D(:,2);
        % Identify a second set of possible outliers.
        f = sqrt(D);
        Q = quantile(f,[0.25,0.75]);
        out2 = f > Q(2) + 1.5*(Q(2)-Q(1));
        % Confirm a point as an outlier if it belongs to both sets of possible
        % outliers (based on distance from origin and distance from nearest
        % neighbour).
        out = out1 & out2;
    else
        out = out1;
    end
    
%     % Check by plotting.
%     % Fig 1: Plot data points in original space
%     figure(1); clf; hold on;
%     if size(Y,2) < 3
%         scatter(Y(~out,1),Y(~out,2),16,'b','filled');
%         scatter(Y(out,1),Y(out,2),16,'r','filled');
%     else
%         scatter3(Y(~out,1),Y(~out,2),Y(~out,3),16,'b','filled');
%         scatter3(Y(out,1),Y(out,2),Y(out,3),16,'r','filled');
%         zlabel('PC3','FontSize',16)
%     end
%     xlabel('PC1','FontSize',16)
%     ylabel('PC2','FontSize',16)
%     set(gca,'fontsize',16);
%     axis equal
%     % Fig 2: Plot data points in the centred and normalized space
%     figure(2); clf; hold on;
%     if size(YY,2) < 3
%         scatter(YY(~out,1),YY(~out,2),16,'b','filled');
%         scatter(YY(out,1),YY(out,2),16,'r','filled');
%     else
%         scatter3(YY(~out,1),YY(~out,2),YY(~out,3),16,'b','filled');
%         scatter3(YY(out,1),YY(out,2),YY(out,3),16,'r','filled');
%         zlabel('PC3','FontSize',16)
%     end
%     xlabel('PC1','FontSize',16)
%     ylabel('PC2','FontSize',16)
%     set(gca,'fontsize',16);
%     axis equal
    
    % Remove outliers from data
    pts = find(pts);
    pts2rmv = pts(out);
    data(pts2rmv,:) = [];
    seed_labels(pts2rmv) = [];
end


if nargin < 6
    % Compute cluster centroids (as means) if not provided as an input argument
    seeds = zeros(max(seed_labels),size(data,2));
    for i = 1:max(seed_labels)
        seeds(i,:) = mean(data(seed_labels==i,:));
    end
    % Specify defaults for function arguments not specified
    if nargin < 5
        plot_shape = false;
        if nargin < 4
            normalizeAS = false;
            if nargin < 3
                DR_method = 'LDA+PCA';
            end
        end
    end
end


%% Identify candidate edges between seeds

% % Test all possible edges:
% [I,J] = find(tril(ones(max(seed_labels)),-1)); 

% Or only test edges that overlap along the dimensions of all PCs computed
% from the seed cluster centroids:
if max(seed_labels)>2
    % Perform PCA on seeds to change basis
    w = pca(seeds);
    % Project data onto PCs calculated for seeds
    W = data * w;
    % For each seed, find the lower and upper bounds along each PC of its
    % constituent data points
    lb = zeros(max(seed_labels),size(w,2));
    ub = zeros(max(seed_labels),size(w,2));
    for i = 1:size(w,2)
        lb(:,i) = arrayfun(@(x) min(W(seed_labels==x,i)), (1:max(seed_labels)).');
        ub(:,i) = arrayfun(@(x) max(W(seed_labels==x,i)), (1:max(seed_labels)).');
    end
    % Find all pairs of seeds with overlapping data point ranges in all of the
    % principle component directions.
    I = cell(max(seed_labels),1);
    J = cell(max(seed_labels),1);
    for i = 2:length(I)
        idx = false(i-1,size(w,2));
        for ii = 1:size(w,2)
            idx(:,ii) = ~( ub(1:i-1,ii) < lb(i,ii) | lb(1:i-1,ii) > ub(i,ii) );
        end
        J{i} = find(prod(idx,2));
        I{i} = i*ones(size(J{i}));
    end
    I = cell2mat(I);
    J = cell2mat(J);
else
    I = 1;
    J = 2;
end

% Convert data into a cell array in which the (i)th cell contains the data 
% points from the (i)th seed.
[seed_labels,idx] = sort(seed_labels,'ascend');
data = data(idx,:);
data = mat2cell(data, arrayfun(@(x) nnz(seed_labels==x), 1:max(seed_labels)), size(data,2));

if nout > 1
    IDX = [I,J];
    % Initialize cell array to store local representations of cluster pairs
    data_local = cell(size(J));
end

%% Determine edge weights

% Set the dimension(s) of the subspaces to project into: 2, 3 or [2,3].
if ndim > 3
    subspace_dim = 2:3;
else
    subspace_dim = ndim;
end

% Initialize edge weights
if ismember(2,subspace_dim)
    S_2D = zeros(size(J));
    S_3D = NaN(size(J)); % Needed for parfor loop to run.
end
if ismember(3,subspace_dim)
    S_3D = zeros(size(J));
    S_2D = NaN(size(J)); % Needed for parfor loop to run.
end

nWorkers = floor(length(J)/10);

% for E = 1:length(J)
parfor (E = 1:length(J), nWorkers)
    Y = []; W = []; % This line is just to make parfor loop run.
    i = I(E);
    j = J(E);
    X = [data{i};data{j}];
    i_idx = 1:size(data{i},1);
    j_idx = (i_idx(end)+1):size(X,1);
    
    if ndim > 3
    
        %% CS step

        % Number of pts in each seed cluster
        Ni = length(i_idx);
        Nj = length(j_idx);

        % Observation weights for PCA so that both seed clusters contribute
        % equally. Must be a column vector.
        if Ni <= Nj
            ObsWeights = [(Nj/Ni)*ones(Ni,1); ones(Nj,1)];
        else
            ObsWeights = [ones(Ni,1); (Ni/Nj)*ones(Nj,1)];
        end

        % Remove any columns with < 2 nonzero elements
        KeepCols = sum(logical(X)) > 1;
        X = X(:,KeepCols);

        forcePCA = false;
        initPCA = false;

        if ~strcmpi(DR_method,'PCA')

            % Initially reduce dimensionality using weighted PCA or SVD if the
            % number of data points is too small.
            n = floor(log2(size(X,1)));
            if n < size(X,2)
                if issparse(X) %|| ( min(X(:))==0 && nnz(X)/numel(X) < 0.5 )
                    % SVD with observation weights
                    [~,~,V] = svds( bsxfun(@times, X, sqrt(ObsWeights)) , n );
                    X = X*V;
                else
                    % If any columns of X have negligible variance, remove them.
                    VarX = var(X);
                    VarCols = VarX > 0.01*mean(VarX);
                    X = X(:,VarCols);
                    % PCA with observation weights
                    [~,X] = pca(X,'NumComponents',n,'Weights',ObsWeights);
                    initPCA = true;
                end
            end
            X = full(X);

            % Compute the vector difference of the cluster centroids
            w = mean(X(i_idx,:)) - mean(X(j_idx,:)); % Compute centroids (extra computation).
            %w = seeds(j,:) - seeds(i,:); % Use input centroids (extra overhead for parfor).

            % Hotelling's two-sample, multivariate T-squared test
            d = size(X,2);
            COVi = cov(X(i_idx,:));
            COVj = cov(X(j_idx,:));
            COVpooled = ((Ni-1)*COVi + (Nj-1)*COVj) / (Ni+Nj-2);
            v = COVpooled\w';
            T2 = Ni*Nj/(Ni+Nj) * w*v;
            Fstat = T2 * (Ni+Nj-d-1) / (d*(Ni+Nj-2));

            % Force use of 'PCA' method for DR if the two cluster means are the
            % same or not significantly different at the 5% level
            Fstat_thresh = finv(0.95,d,Ni+Nj-d);
            forcePCA = Fstat < Fstat_thresh || norm(w)<=eps || size(X,2)<4;

            if strcmpi(DR_method,'centroids+PCA') && ~forcePCA

                % Find the direction through the centroids of both seed clusters.
                w = w ./ norm(w);

                % Project the data X from the two seeds onto w and express in the basis
                % of w.
                W = X * w';

                % Find the projection of X onto the orthogonal complement of w
                % expressed in the original basis of X (i.e. subtract the component of
                % each row in X that is in the direction of w).
                X = X - W*w;   

            elseif strcmpi(DR_method,'LDA+PCA') && ~forcePCA

    %             % Find the direction through the centroids of both seed clusters.
    %             w = w ./ norm(w);
    % 
    %             % Find the within-class scatter matrices
    %             Sc1 = Ni*cov(X(i_idx,:));
    %             Sc2 = Nj*cov(X(j_idx,:));

                % Find Fisher's linear discriminant
    %             LD = (Sc1+Sc2)\w';
    %             LD = LD ./ norm(LD);
                LD = v./ norm(v);   % Using the vector v computed using the slightly
                                    % different pooled covariance matrix from
                                    % Hotelling's T-squared test.

                % Project the data X from the two seeds onto LD and express in the
                % basis of LD.
                W = X * LD;

                % Find the projection of X onto the orthogonal complement of LD
                % expressed in the original basis of X (i.e. subtract the component
                % of each row in X that is in the direction of LD).
                X = X - W*LD';

            elseif forcePCA
                % Use PCs only instead
                if initPCA
                    Y = X(:,1:max(subspace_dim));
                else
                    [~,Y] = pca(X,'NumComponents',max(subspace_dim),'Weights',ObsWeights);
                end
            else
                error('Invalid string for DR_method.')
            end

        else % if strcmpi(DR_method,'PCA')
            % Find the first 2 or 3 principal components for the data in the two seeds
            % corresponding to edge E and project the data onto the 2D or 3D subspace
            % spanned by these 2 or 3 PCs.
            [~,Y] = pca(full(X),'NumComponents',max(subspace_dim),'Weights',ObsWeights);
        end

        if ~strcmpi(DR_method,'PCA') && ~forcePCA
            % Find the first 1 or 2 principal components (PCs) of the projected
            % X and project the data onto the 1D or 2D subspace spanned by these PCs
            [~,Y] = pca(X,'NumComponents',max(subspace_dim)-1,'Weights',ObsWeights);

            % Use the span of w or LD and the PCs in Y as the 2D or 3D subspace in
            % which to compute the similarity of seed clusters i and j corresponding
            % to edge E.
            Y = [W-mean(W),Y];
        end
        
    else % if data only has 2 or 3 dimensions... 
        % Just use the original data.
        Y = full(X);
    end
    
    if nout > 1
        data_local{E} = Y;
    end
    
    %% Check for and remove any duplicate rows from Y 
    %  (possibly due to "data piling" after DR)
    [Y_tmp,ia,~] = unique(Y,'rows');
    i_tmp = find(ismember(ia,i_idx));
    j_tmp = find(ismember(ia,j_idx));
       
    for dim = subspace_dim
        if length(subspace_dim) > 1
            if (dim==3 && subspace_dim(1)==2 && S_2D(E)==0) || ...
                    (dim==2 && subspace_dim(1)==3 && S_3D(E)==0)
                continue;
            end
        end
        
        if dim < max(subspace_dim)
            %% Check for and remove any duplicate rows from Y(:,1:dim)
            %  (possibly due to "data piling" after DR)
            [Y,ia,~] = unique(Y_tmp(:,1:dim),'rows');
            i_idx = find(ismember(ia,i_tmp));
            j_idx = find(ismember(ia,j_tmp));
        else
            Y = Y_tmp;
            i_idx = i_tmp;
            j_idx = j_tmp;
        end
        
        %% ASTRI step
        % Set the edge weight between two seeds equal to the fraction of
        % mesh elements that have vertex points from both seeds in a
        % triangulation of an alpha shape computed for all points in the
        % union of both seeds in a subspace defined by the first 2 or 3
        % principal components (PCs). The chosen alpha shape is the
        % smallest alpha shape that encloses all of the points from at
        % least one of the two seed clusters.
        
        % Compute the smallest alpha shape enclosing all points in Y
        % (possibly split into multiple regions).
        SHP = alphaShape(Y(:,1:dim));
        
        % Get the spectrum of alpha values resulting in unique alpha shapes
        ASpec = alphaSpectrum(SHP);
        % Choose the smallest value of alpha from the spectrum that yields
        % an alpha shape enclosing all of the points from at least one of
        % the two seed clusters.
        idx1 = find(ASpec==SHP.Alpha);
        idx3 = numel(ASpec);
        while true
            idx2 = ceil(0.5*(idx1+idx3));
            if idx2==idx3
                SHP.Alpha = ASpec(idx1);
                break;
            end
            SHP.Alpha = ASpec(idx2);
            NumPtsExcl_i = sum(~inShape(SHP,Y(i_idx,1:dim)));
            NumPtsExcl_j = sum(~inShape(SHP,Y(j_idx,1:dim)));
            if NumPtsExcl_i < 1 || NumPtsExcl_j < 1
                idx1 = idx2;
            else
                idx3 = idx2;
            end

%             %% Check by plotting
%             % Fig 1
%             shp1 = SHP;
%             shp1.Alpha = ASpec(idx1);
%             figure(1);
%             cla; hold off;
%             plot(shp1,'FaceAlpha',0.5,'FaceColor',[1,0,1],'EdgeAlpha',0.25);
%             hold on;
%             if dim < 3
%                 scatter(Y(i_idx,1),Y(i_idx,2),16,'r','filled');
%                 scatter(Y(j_idx,1),Y(j_idx,2),16,'b','filled');
%             else
%                 scatter3(Y(i_idx,1),Y(i_idx,2),Y(i_idx,3),16,'r','filled');
%                 scatter3(Y(j_idx,1),Y(j_idx,2),Y(j_idx,3),16,'b','filled');
%                 zlabel('PC3','FontSize',16)
%             end
%             xlabel('PC1','FontSize',16)
%             ylabel('PC2','FontSize',16)
%             set(gca,'fontsize',16);
%             axis equal
%             % Fig 2
%             shp3 = SHP;
%             shp3.Alpha = ASpec(idx3);
%             figure(2);
%             cla; hold off;
%             plot(shp3,'FaceAlpha',0.5,'FaceColor',[1,0,1],'EdgeAlpha',0.25);
%             hold on;
%             if dim < 3
%                 scatter(Y(i_idx,1),Y(i_idx,2),16,'r','filled');
%                 scatter(Y(j_idx,1),Y(j_idx,2),16,'b','filled');
%             else
%                 scatter3(Y(i_idx,1),Y(i_idx,2),Y(i_idx,3),16,'r','filled');
%                 scatter3(Y(j_idx,1),Y(j_idx,2),Y(j_idx,3),16,'b','filled');
%                 zlabel('PC3','FontSize',16)
%             end
%             xlabel('PC1','FontSize',16)
%             ylabel('PC2','FontSize',16)
%             set(gca,'fontsize',16);
%             axis equal

        end

        % Find a triangulation that defines the domain of the alpha shape.
        atri = alphaTriangulation(SHP);
        
%         % Compute the volumes or areas of all triangulation elements.
%         if dim==2
%             V = 0.5 * arrayfun(@(a,b,c)...
%                 norm(cross( [Y(a,1:dim)-Y(c,1:dim),0] , [Y(b,1:dim)-Y(c,1:dim),0] )),...
%                 atri(:,1), atri(:,2), atri(:,3) );
%         elseif dim==3
%             V = (1/6) * arrayfun(@(a,b,c,d)... 
%                 abs(det([ Y(a,1:dim)-Y(d,1:dim) ; Y(b,1:dim)-Y(d,1:dim) ; Y(c,1:dim)-Y(d,1:dim) ])),...
%                 atri(:,1), atri(:,2), atri(:,3), atri(:,4) );
%         else
%             error('Invalid number of subspace dimensions. Must be 2 or 3.');
%         end
        
        % Find triangulation elements (triangles in 2D or tetrahedra in 3D)
        % that contain points from both seed i and seed j.
        mixed_elements = any(ismember(atri,i_idx),2) & any(ismember(atri,j_idx),2);
        
        % Set the weight of edge E = (seed i, seed j) equal to the fraction
        % of triangulation elements that contain points from both seeds i
        % and j, optionally weighted by their inverse volumes (i.e. densities).
        if dim==3
            S_3D(E) = nnz(mixed_elements) / size(atri,1);
            %S_3D(E) = sum(mixed_elements./V) / sum(1./V);
        elseif dim==2
            S_2D(E) = nnz(mixed_elements) / size(atri,1);
            %S_2D(E) = sum(mixed_elements./V) / sum(1./V);
        else
            error('Invalid number of subspace dimensions. Must be 2 or 3.');
        end


        %% Plotting
        if plot_shape %dim==2 && i==20 && j==9
            disp(['Edge = (' num2str(I(E)) ',' num2str(J(E)) ')']);
            % Plot points and alpha shapes to visually check results
            if dim < 3
                disp(['Edge weight = ' num2str(S_2D(E))]);
            else
                disp(['Edge weight = ' num2str(S_3D(E))]);
            end
            labs = [ones(numel(i_idx),1); 2*ones(numel(j_idx),1)];
            % Fig
            figure(dim-1); 
            cla; 
%             hold off;
%             plot(SHP,'FaceAlpha',0.5,'FaceColor',[1,0,1],'EdgeAlpha',0.25);
            hold on;
            set(gca,'fontsize',16);
            if dim < 3
                redtri = all(ismember(atri,i_idx),2);
                bluetri = all(ismember(atri,j_idx),2);
                patch('Faces',atri(redtri,:),'Vertices',Y(:,1:2),'FaceColor','y','FaceAlpha',0.8);
                patch('Faces',atri(bluetri,:),'Vertices',Y(:,1:2),'FaceColor','c','FaceAlpha',0.8);
                patch('Faces',atri(mixed_elements,:),'Vertices',Y(:,1:2),'FaceColor','m','FaceAlpha',0.8);
                scatter(Y(:,1),Y(:,2),18,labs,'filled');
            else
                plot(SHP,'FaceAlpha',0.5,'FaceColor',[1,0,1],'EdgeAlpha',0.25);
                scatter3(Y(:,1),Y(:,2),Y(:,3),16,labs,'filled');
                zlabel('Dim 3','FontSize',16)
            end
%             xlabel('Dim 1','FontSize',16)
%             ylabel('Dim 2','FontSize',16)
            xlabel('Subspace dimension 1','FontSize',18)
            ylabel('Subspace dimension 2','FontSize',18)
            colormap([1,0,0;0,0,1])
            %colorbar
            axis equal
            %keyboard; % Wait for keyboard input (equivalent to a breakpoint)
        end

    end
end

%% Take the minimum of the 2D and 3D edge weights (if both were computed)
use2D = ismember(2,subspace_dim);
use3D = ismember(3,subspace_dim);
if use2D && use3D
    S = min(S_2D,S_3D);
elseif use3D && ~use2D
    S = S_3D;
elseif use2D && ~use3D
    S = S_2D;
else
    error('There are no edge weights!')
end

%% Construct adjacency matrix for graph
N = max(seed_labels);
idx = S > 0;
I = I(idx);
J = J(idx);
S = S(idx);
if normalizeAS && ~isempty(S)
    % Rescale edge weights by a constant factor so that the largest weight is
    % always 1 (shouldn't affect most clustering algorithms other than
    % precision error, but helps visualization of force-directed layout graph).
    S = S./max(S);
end
AS = sparse([I;J],[J;I],[S;S],N,N);

end